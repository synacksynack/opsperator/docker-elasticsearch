FROM opsperator/java

# ElasticSearch image for OpenShift Origin

ARG DO_UPGRADE=
ENV BASE_URL=https://artifacts.elastic.co/downloads/elasticsearch \
    PATH="/usr/share/elasticsearch/bin:$PATH" \
    VERSION=8.6.0

LABEL io.k8s.description="ElasticSearch" \
      io.k8s.display-name="ElasticSearch $VERSION" \
      io.openshift.expose-services="9200:elasticsearch,9300:nodes" \
      io.openshift.tags="elasticsearch" \
      io.openshift.non-scalable="false" \
      help="For more information visit https://gitlab.com/synacksynack/opsperator/docker-elasticsearch" \
      maintainer="Samuel MARTIN MORO <faust64@gmail.com>" \
      version="$VERSION"

USER root

COPY config/* /

RUN set -x \
    && apt-get update \
    && if test "$DO_UPGRADE"; then \
	echo "# Upgrade Base Image"; \
	apt-get -y upgrade; \
	apt-get -y dist-upgrade; \
    fi \
    && echo "# Install ElasticSearch Dependencies" \
    && apt-get install -y --no-install-recommends wget \
	zip unzip procps findutils curl \
    && echo "# Install ElasticSearch" \
    && ESARCH=`uname -m` \
    && if ! echo "$ESARCH" | grep -E '(x86_64|aarch64)' >/dev/null; then \
	echo CRITICAL: unsupported architecture \
	&& exit 1; \
    fi \
    && wget --no-check-certificate \
	-O- "$BASE_URL/elasticsearch-$VERSION-linux-$ESARCH.tar.gz" \
        | tar -C /usr/share -xzf - \
    && ln -s /usr/share/elasticsearch-$VERSION /usr/share/elasticsearch \
    && mkdir -p /var/lib/elasticsearch /var/lib/java-shared \
    && ln -sf /var/lib/java-shared/ /javasharedresources \
    && mv /escli /usr/local/bin/ \
    && echo "# Fixing Permissions" \
    && chown -R 1001:root /usr/share/elasticsearch-$VERSION \
	/var/lib/elasticsearch \
    && chmod -R g=u /usr/share/elasticsearch-$VERSION /var/lib/elasticsearch \
    && echo "# Cleaning Up" \
    && apt-get clean -y \
    && apt-get autoremove -y --purge \
    && rm -rf /var/lib/apt/lists/* /usr/share/doc /usr/share/man \
    && unset HTTP_PROXY HTTPS_PROXY NO_PROXY DO_UPGRADE http_proxy https_proxy

USER 1001
WORKDIR /var/lib/elasticsearch
ENTRYPOINT ["dumb-init","--","/run-elasticsearch.sh"]
CMD "/usr/share/elasticsearch/bin/elasticsearch"
