#!/bin/sh

if test "$DEBUG"; then
    set -x
fi
RUNTIME_USER=elasticsearch
. /usr/local/bin/nsswrapper.sh

if test -z "$HOSTNAME"; then
    HOSTNAME=`hostname`
fi
ELASTICSEARCH_CLUSTER_NAME=${ELASTICSEARCH_CLUSTER_NAME:-k8s}
ELASTICSEARCH_DATA_DIR=${ELASTICSEARCH_DATA_DIR:-/var/lib/elasticsearch}
ELASTICSEARCH_HAS_MASTERS=${ELASTICSEARCH_HAS_MASTERS:-1}
ELASTICSEARCH_JVM_XMS=${ELASTICSEARCH_JVM_XMS:-512m}
ELASTICSEARCH_JVM_XMX=${ELASTICSEARCH_JVM_XMX:-512m}
ELASTICSEARCH_SERVER_ID=`echo "$HOSTNAME" | sed 's|^.*-\([0-9][0-9]*\)$|\1|'`

if test -z "$POD_IP"; then
    POD_IP=$(cat /proc/net/fib_trie | grep "|--"  \
		| egrep -v "(0.0.0.0| 127.|.255$|.0$)" \
		| awk '{print $NF}' | head -1)
fi

gen_masters()
{
    local check

    check=0
    while test "$check" -lt "$ELASTICSEARCH_HAS_MASTERS"
    do
	if test "$ELASTICSEARCH_SEEDS"; then
	    export ELASTICSEARCH_SEEDS="$ELASTICSEARCH_SEEDS
- $ELASTICSEARCH_MASTER_SVC-$check.$ELASTICSEARCH_MASTER_SVC"
	else
	    export ELASTICSEARCH_SEEDS="- $ELASTICSEARCH_MASTER_SVC-$check.$ELASTICSEARCH_MASTER_SVC"
	fi
	check=`expr $check + 1`
    done
}

if test -z "$ELASTICSEARCH_IS_MASTER" -a -z "$ELASTICSEARCH_IS_DATA" \
	-a -z "$ELASTICSEARCH_IS_INGEST"; then
    ELASTICSEARCH_IS_DATA=true
    ELASTICSEARCH_IS_INGEST=true
    ELASTICSEARCH_IS_MASTER=true
else
    for k in MASTER DATA INGEST
    do
	eval "v=\$ELASTICSEARCH_IS_$k"
	if echo "$v" | grep -E '^(t|y)' >/dev/null; then
	    eval "ELASTICSEARCH_IS_$k=true"
	else
	    eval "ELASTICSEARCH_IS_$k=false"
	fi
    done
fi

if ! test "$ELASTICSEARCH_SERVER_ID" = "$HOSTNAME"; then
    ELASTICSEARCH_BASENAME=`echo $HOSTNAME | sed "s|-$ELASTICSEARCH_SERVER_ID$||"`
    ELASTICSEARCH_MASTER_SVC=$ELASTICSEARCH_BASENAME
fi
if test "$ELASTICSEARCH_MASTER_SVC"; then
    gen_masters
else
    export ELASTICSEARCH_SEEDS="- $HOSTNAME"
fi
if $ELASTICSEARCH_IS_MASTER; then
    ELASTICSEARCH_NODENAME=$ELASTICSEARCH_MASTER_SVC-$ELASTICSEARCH_SERVER_ID.$ELASTICSEARCH_MASTER_SVC
else
    ELASTICSEARCH_NODENAME=$HOSTNAME
fi

sed -e "s|CLUSTER_NAME|$ELASTICSEARCH_CLUSTER_NAME|" \
    -e "s|DATA_DIR|$ELASTICSEARCH_DATA_DIR|" \
    -e "s|NODE_NAME|$ELASTICSEARCH_NODENAME|" \
    /elasticsearch.yml \
    >/usr/share/elasticsearch/config/elasticsearch.yml

cat <<EOF >>/usr/share/elasticsearch/config/elasticsearch.yml
cluster.initial_master_nodes:
$ELASTICSEARCH_SEEDS
discovery.seed_hosts:
$ELASTICSEARCH_SEEDS
EOF

if "$ELASTICSEARCH_IS_MASTER"; then
    cat <<EOF >>/usr/share/elasticsearch/config/opensearch.yml
- master
EOF
fi
if "$ELASTICSEARCH_IS_INGEST"; then
    cat <<EOF >>/usr/share/elasticsearch/config/opensearch.yml
- ingest
EOF
fi
if "$ELASTICSEARCH_IS_DATA"; then
    cat <<EOF >>/usr/share/elasticsearch/config/opensearch.yml
- data
EOF
fi
if test "$ELASTICSEARCH_DO_CORS"; then
    cat <<EOF >>/usr/share/elasticsearch/config/elasticsearch.yml
http.cors.enabled: true
http.cors.allow-origin: "*"
EOF
fi

sed -e "s|XMS|$ELASTICSEARCH_JVM_XMS|" \
    -e "s|XMX|$ELASTICSEARCH_JVM_XMX|" \
    /jvm.options \
    >/usr/share/elasticsearch/config/jvm.options

if uname -m | grep aarch64 >/dev/null; then
    sed -i "/UseAVX=/d" /usr/share/elasticsearch/config/jvm.options
fi

if test "$ELASTICSEARCH_PLUGINS"; then
    for plugin in $ELASTICSEARCH_PLUGINS
    do
	if ! test -d /usr/share/elasticsearch/plugins/$plugin; then
	    echo y | \
		/usr/share/elasticsearch/bin/elasticsearch-plugin \
		install $plugin
	fi
    done
fi

cat <<EOF
Starting ElasticSearch node
is_data:$ELASTICSEARCH_IS_DATA is_ingest:$ELASTICSEARCH_IS_INGEST is_master:$ELASTICSEARCH_IS_MASTER
has_masters: $ELASTICSEARCH_HAS_MASTERS
xms:$ELASTICSEARCH_JVM_XMS xmx:$ELASTICSEARCH_JVM_XMX
EOF
if test "$DEBUG"; then
    cat /usr/share/elasticsearch/config/elasticsearch.yml
fi

unset ELASTICSEARCH_CLUSTER_NAME ELASTICSEARCH_DATA_DIR ELASTICSEARCH_HAS_MASTERS \
    ELASTICSEARCH_JVM_XMS ELASTICSEARCH_JVM_XMX ELASTICSEARCH_SERVER_ID \
    ELASTICSEARCH_IS_DATA ELASTICSEARCH_IS_MASTER ELASTICSEARCH_IS_INGEST POD_IP \
    ELASTICSEARCH_MASTER_SVC ELASTICSEARCH_BASENAME ELASTICSEARCH_NODENAME

exec "$@"
