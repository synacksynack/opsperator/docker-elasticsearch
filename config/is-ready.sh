#!/bin/sh

if test "$DEBUG"; then
    set -x
fi
if test -z "$POD_IP"; then
    POD_IP=$(cat /proc/net/fib_trie | grep "|--"  \
		| egrep -v "(0.0.0.0| 127.|.255$|.0$)" \
		| awk '{print $NF}' | head -1)
fi
if test -z "$ELASTICSEARCH_IS_MASTER" -a -z "$ELASTICSEARCH_IS_DATA" \
	-a -z "$ELASTICSEARCH_IS_INGEST"; then
    ELASTICSEARCH_IS_MASTER=true
fi
if echo "$ELASTICSEARCH_IS_MASTER" | grep -E '^(t|y)' >/dev/null; then
    MAGIC="_cluster/health?wait_for_status=yellow&timeout=3s"
    wget -O- "http://$POD_IP:9200/$MAGIC" \
	2>/dev/null | grep -Ei '"status":"(yellow|green)"' >/dev/null
    exit $?
fi

wget "http://$POD_IP:9200/" -O-
exit $?
