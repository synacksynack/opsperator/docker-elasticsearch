# k8s ElasticSearch

OpenShift-friendly ElasticSearch Image

Building Clusters, make sure to set `ELASTICSEARCH_HAS_MASTERS` to
the minimum amount of Pods our masters statefulset would hold. Although
note that whatever value would work, it would be required ensuring proper
nodes detection during an outage.

The provided template is merely an example of how ElasticSearch can be
deployed into OpenShift. We could use a single statefulset hosting
masters and data nodes as well, although moving data nodes away seems
more fitting scaling out specific services. Ingest nodes could be moved
to a separate statefulset as well.

Environment variables
----------------------

The image recognizes the following environment variables that you can set during
initialization by passing `-e VAR=VALUE` to the Docker `run` command.

|    Variable name                   |    Description                            | Default                    |
| :--------------------------------- | ----------------------------------------- | -------------------------- |
|  `ELASTICSEARCH_CLUSTER_NAME`      | ElasticSearch Cluster Name                | `k8s`                      |
|  `ELASTICSEARCH_DATA_DIR`          | ElasticSearch Data Directory              | `/var/lib/elasticsearch`   |
|  `ELASTICSEARCH_DO_CORS`           | ElasticSearch Toggles Cors Configuration  | undef                      |
|  `ELASTICSEARCH_HAS_MASTERS`       | ElasticSearch Expected Masters Amount     | `1`                        |
|  `ELASTICSEARCH_IS_DATA`           | ElasticSearch Works as Data Node          | undef                      |
|  `ELASTICSEARCH_IS_INGEST`         | ElasticSearch Works as Ingest Node        | undef                      |
|  `ELASTICSEARCH_IS_MASTER`         | ElasticSearch Works as Master Node        | undef                      |
|  `ELASTICSEARCH_JVM_XMS`           | ElasticSearch JVM XMS Option              | `512m`                     |
|  `ELASTICSEARCH_JVM_XMX`           | ElasticSearch JVM XMX Option              | `512m`                     |
|  `ELASTICSEARCH_MIN_MASTER`        | ElasticSearch Minimum Masters to Wait for | deduced from `HAS_MASTERS` |
|  `ELASTICSEARCH_PING_INTERVAL`     | ElasticSearch Ping Interval               | `1s`                       |
|  `ELASTICSEARCH_PING_RETRIES`      | ElasticSearch Ping Retries                | `3`                        |
|  `ELASTICSEARCH_PING_TIMEOUT`      | ElasticSearch Ping Timeout                | `30s`                      |
|  `ELASTICSEARCH_PLUGINS`           | ElasticSearch Plugins to Install          | undef                      |
|  `POD_IP`                          | ElasticSearch Listen Address              | detected                   |

Knowing that if none of `IS_MASTER`, `IS_INGEST` nor `IS_DATA` were set, then
all three would default to `true`. Despite ElasticSearch documentation
suggesting a node could still be part of a cluster without assuming any of these
functions, we would not allow this mode, for "simplicity".

Volumes
--------

|  Volume mount point           | Description                   |
| :---------------------------- | :---------------------------- |
|  `/var/lib/elasticsearch`     | ElasticSearch Data Directory  |
